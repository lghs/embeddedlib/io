

#include "ReedSwitch.h"

void ReedSwitch::setup() {
    pinMode(pin, INPUT);
}

void ReedSwitch::run() {
    int state = (pulledUp)?!digitalRead(pin): digitalRead(pin);
    if (lastState != state) {
        Serial.print("reed on pin ");
        Serial.print(pin);
        Serial.print(" - ");
        Serial.println(state);

        lastStateChange = millis();
        lastState = state;
    }
    if (lastState == state && hasContact != state) {
        if ((millis() - lastStateChange) > REED_SWITCH_DEBOUNCE_DURATION) {
            hasContact = state;
            Serial.print("reed has contact on pin ");
            Serial.print(pin);
            Serial.print(" - ");
            Serial.println(hasContact);
        }
    }

}

bool ReedSwitch::inContact() {
    run();
    return hasContact;
}

