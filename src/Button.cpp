//
// Created by francois on 27.04.21.
//

#include "Button.h"

Button::Button(int pin, bool pulledUp) : _buttonPin(pin), pulledUp(pulledUp) {
}

Button::~Button() {
}

void Button::setup(int mode) {
    pinMode(_buttonPin, mode);
}

void Button::run() {
    int buttonState = digitalRead(_buttonPin);
    if (_lastButtonState != buttonState) {
        Serial.print("button on pin ");
        Serial.print(_buttonPin);
        Serial.print(" - ");
        Serial.println(buttonState);

        if (buttonState == releasedState() &&
            (millis() - _lastButtonChange) > BUTTON_DEBOUNCE_DURATION &&
            _lastPopedLongClick == -1) {
            clickToPop = true;
        } else if (buttonState == pressedState()) {
            _lastPopedLongClick = -1;
        }
        _lastButtonState = buttonState;
        _lastButtonChange = millis();
    }
}

int Button::pressedState() { return (pulledUp) ? LOW : HIGH; }
int Button::releasedState() { return (pulledUp) ? HIGH : LOW; }

bool Button::popClicked() {
    run();
    if (clickToPop) {
        clickToPop = false;
        return true;
    } else {
        return false;
    }
}

bool Button::popLong(long duration) {
    run();
    long pressDuration = millis() - _lastButtonChange;
    if(_lastButtonState == pressedState() && _lastPopedLongClick < duration && pressDuration >= duration ){
        _lastPopedLongClick = duration;
        return true;
    } else {
        return false;
    }

}

bool Button::inLong(long duration) {
    popLong(duration);
    if(_lastButtonState == pressedState() && _lastPopedLongClick == duration){
        return true;
    } else {
        return false;
    }
}
