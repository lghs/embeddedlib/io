

#pragma once
#include <Arduino.h>

#define REED_SWITCH_DEBOUNCE_DURATION 20

class ReedSwitch {
    int pin;
    bool pulledUp;
    bool lastState = 0;
    long lastStateChange = 0;
    bool hasContact = 0;
public:
    ReedSwitch(int pin, bool pulledUp=true) : pin(pin), pulledUp(pulledUp){};
    void setup();
    void run();
    bool inContact();

};



