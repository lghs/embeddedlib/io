#ifndef BUTTON_H
#define BUTTON_H

#include "Arduino.h"

#define BUTTON_DEBOUNCE_DURATION 20

/**
 * button class that register press on the button
 * a press is registered once the button have been released
 */
class Button {
private:
    int _buttonPin;
    char _lastButtonState = LOW;
    bool clickToPop = false; //true if a button press has been registered
    long _lastButtonChange = 0;
    long _lastPopedLongClick = -1;
    bool pulledUp = true;

    int pressedState();

    int releasedState();

public:
    Button(int pin, bool pulledUp = true);

    ~Button();

    void run();

    void setup(int mode = INPUT_PULLUP);

    /**
     * @return true if a press has been registered
     */
    bool popClicked();


    bool popLong(long duration);

    bool inLong(long duration);

};


#endif